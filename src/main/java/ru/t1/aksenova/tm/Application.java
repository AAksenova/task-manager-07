package ru.t1.aksenova.tm;

import ru.t1.aksenova.tm.constant.ArgumentConst;
import ru.t1.aksenova.tm.constant.CommandConst;
import ru.t1.aksenova.tm.model.Command;
import ru.t1.aksenova.tm.util.FormatUtil;

import java.util.Scanner;

import static ru.t1.aksenova.tm.constant.CommandConst.*;

public class Application {

    public static void main(final String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()){
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    public static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
            break;
            case ArgumentConst.VERSION:
                showVersion();
            break;
            case ArgumentConst.HELP:
                showHelp();
            break;
            case ArgumentConst.INFO:
                showInfo();
                break;
            default:
                showArgumentError();
            break;
        }
    }

    public static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.INFO:
                showInfo();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                showCommandError();
                break;
        }
    }

    public static void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usageMemory = totalMemory - freeMemory;

        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);

        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;

        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Usage memory: " + usageMemoryFormat );
    }

    public static void exit() {
        System.exit(0);
    }

    public static void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument not supported...");
        System.exit(1);
    }

    public static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command not supported...");
        System.exit(1);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Anastasiya Aksenova");
        System.out.println("e-mail: aaksenova@t1-consulting.ru");
        System.out.println("e-mail: cs.aksenova@gmail.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(Command.ABOUT);
        System.out.println(Command.VERSION);
        System.out.println(Command.HELP);
        System.out.println(Command.INFO);
        System.out.println(Command.EXIT);
    }

}
